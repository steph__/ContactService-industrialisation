<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

use PHPUnit\Framework\TestCase;

require_once __DIR__.'/../../src/ContactService.php';

/**
 * * @covers invalidInputException
 * @covers \ContactService
 *
 * @internal
 */
final class ContactServiceIntegrationTest extends TestCase
{
    private $contactService;

    public function __construct(string $name = null, array $data = [], $dataName = '') {
        parent::__construct($name, $data, $dataName);
        $this->contactService = new ContactService();
    }

    public function testCreationContact()
    {
        $this->contactService->createContact('testNom', 'testPrenom');
        $data = $this->contactService->getAllContacts();
        $response = end($data);
        // echo "Creation contact :";
        // echo var_dump($data);
        static::assertSame('testNom', $response['nom']);
        static::assertSame('testPrenom', $response['prenom']);
        // $this->id = $data[0]['id'];

    }

}
